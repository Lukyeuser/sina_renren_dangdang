# 新浪网的分类导航_人人网登陆_爬取当当图书网站

#### 项目介绍
1）.使用Scrapy爬虫框架爬取新浪网的分类导航信息： 
网址：http://news.sina.com.cn/guide/ 
2 ). 使用scrapy模拟完成人人网登陆的登陆操作: 
如URL地址：http://www.renren.com/ 
3）. 爬取当当图书网站中所有关于python关键字的图片信息。 
参考URL：http://search.dangdang.com/?key=python&act=input 
要求：将图书图片下载存储指定的目录中，而图书信息写入到数据库中。 

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)